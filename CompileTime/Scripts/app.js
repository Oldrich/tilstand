/// <reference path="./references.ts" />
var App;
(function (App) {
    var pageDef = $.Deferred().resolve();
    var scrolls = {};

    function fadeIn(h, df, duration) {
        localStorage.setItem("lastPage", h[0]);
        $(h[0]).trigger('page-loaded', h[1]);
        if (scrolls[h[0]] && duration === 0) {
            $('body').height(scrolls[h[0]].bodyHeight);
            var dy = window.innerHeight - scrolls[h[0]].windowHeight;
            var ddy = 0;

            //if (dy === 69) { ddy = 25; } else if (dy === -69) { ddy = -25; }
            //if (dy === 108) { ddy = 30; } else if (dy === -108) { ddy = -30; }
            window.scrollTo(0, scrolls[h[0]].y - ddy);
        } else {
            window.scrollTo(0, 0);
        }
        $(h[0]).fadeIn(duration, function () {
            $(h[0]).trigger('page-shown');
            df.resolve(h[0]);
        });
    }

    function changePage(hash, duration) {
        if (typeof duration === "undefined") { duration = 300; }
        hash = hash || "";
        pageDef.then(function (lastId) {
            pageDef = $.Deferred();
            var h = hash.split('/', 2);
            if ($(h[0]).length === 0) {
                pageDef.resolve();
                goto("#home");
            } else if (!lastId) {
                fadeIn(h, pageDef, 0);
            } else if (h[0] !== lastId) {
                $(lastId).find('.searchWrapper > input').hide().val('');
                scrolls[lastId] = {
                    y: $(window).scrollTop(),
                    bodyHeight: $('body').height(),
                    windowHeight: window.innerHeight
                };
                $(lastId).fadeOut(duration, function () {
                    fadeIn(h, pageDef, duration);
                });
            } else {
                pageDef.resolve(lastId);
            }
        });
    }

    function goto(path) {
        window.history.pushState(path, '', path);
        changePage(path);
    }
    App.goto = goto;

    //search
    function searchButton() {
        $('.page').each(function (i, el) {
            var input = $(el).find('.searchWrapper > input');
            input.focusout(function () {
                var query = input.val();
                if (query.replace(/^\s+|\s+$/g, '').length > 0) {
                    goto("#search/" + encodeURIComponent(query));
                } else {
                    input.hide();
                    input.val('');
                }
            });
            $(el).find('.searchButton').click(function () {
                input.show();
                input.focus();
            });
            $(el).find('.searchWrapper').submit(function (e) {
                goto("#search/" + encodeURIComponent($(el).find('.searchWrapper > input').val()));
                e.preventDefault();
            });
        });
    }

    //img link
    function resize(left, top, imgj, link) {
        return function (e) {
            var dx = imgj.offset().left - imgj.parent().offset().left;
            var x = left * imgj.width() + dx;
            var y = top * imgj.height();
            link.css('left', x);
            link.css('top', y);
            link.fadeIn(500);
        };
    }

    function createImgLink(img, pageId) {
        return function (left, top, text, url) {
            var imgj = $(img);
            var inner = url ? '<a class="imgLink" href = "' + url + '" >' + text + '</a>' : '<div class="imgMarker">' + text + '</div>';
            var link = $('<div class="imgLink-wrapper">' + inner + '</div>');
            var r = resize(left, top, imgj, link);
            imgj.before(link);
            $(window).resize(r);
            $(pageId).on('page-shown', r);
        };
    }
    App.createImgLink = createImgLink;

    function changeLinks() {
        $('a:not([onclick])').click(function (e) {
            goto($(this).attr('href'));
            return false;
        });
    }

    function moveNavigationBar() {
        $('.navigate').css('left', window.innerWidth / 2 - 65);
    }

    $(window).load(function () {
        if ((window).navigator.standalone && localStorage.getItem("lastPage")) {
            goto(localStorage.getItem("lastPage"));
        }
        searchButton();
        changeLinks();
        moveNavigationBar();
    });

    $(window).resize(moveNavigationBar);

    window.addEventListener("popstate", function () {
        if ((window).navigator.standalone) {
            changePage(history.state, 300);
        } else {
            changePage(history.state, 0);
        }
    });

    window.applicationCache.addEventListener('updateready', function (e) {
        window.applicationCache.swapCache();
    }, false);
})(App || (App = {}));
//# sourceMappingURL=app.js.map
