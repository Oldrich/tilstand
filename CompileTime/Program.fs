﻿open System.IO
open System.Xml.Linq
open System.Text.RegularExpressions
open System.Collections.Generic

let rec tryDelete(dir: string) =
  //Directory.GetFiles dir |> Array.iter (fun file -> try File.Delete file with _ -> ())
  Directory.GetDirectories dir |> Array.iter tryDelete

let root = @"c:\Users\osv\Documents\Sync\DTI\handbog\2013 tilstand\web\web\"
let what = "sfrc"
let pagesDir = DirectoryInfo(Path.Combine(root, "userData", what))
let odir = Path.Combine(root, "..\\.." , "deploy", what)
if Directory.Exists odir then tryDelete odir
let outDir = Directory.CreateDirectory(odir)

let tryGet what dir =
  let path = Path.Combine(dir, "meta.xml")
  if File.Exists path then
    let meta = XDocument.Parse(File.ReadAllText path)
    let d = meta.Descendants(XName.Get what)
    if Seq.length d > 0 then
      Some ((d |> Seq.head).Value.Trim())
    else None
  else None

let get what dir = (tryGet what dir).Value

let rec getDirIdR (dir: DirectoryInfo) =
  if dir.FullName = pagesDir.FullName then ""
  else getDirIdR (dir.Parent) + "_" + dir.Name

let getDirId (dir: DirectoryInfo) =
  let id = getDirIdR dir
  if id = "" then "home" else id.Remove(0,1)

let inlineScripts(text: string) =
  Regex.Replace(text, "{{script: *([^}]*) *}}", fun v ->
    let path = v.Groups.[1].Value
    let text = File.ReadAllText(Path.Combine(pagesDir.Parent.FullName, path))
    sprintf "<script>%s</script>" text
  )

let inlineStyles(text: string) =
  Regex.Replace(text, "{{style: *([^}]*) *}}", fun v ->
    let path = v.Groups.[1].Value
    let text = File.ReadAllText(Path.Combine(pagesDir.Parent.FullName, path))
    sprintf "<style>%s</style>" text
  )

let tryGetMainImgSrc (dir: DirectoryInfo) =
  let imFiles = dir.GetFiles("image.*")
  if imFiles.Length > 0 then
    let path = sprintf "./Pages/%s/%s" (getDirId(dir).Replace("_", "/")) imFiles.[0].Name
    path, dir
  else
    let dirs = dir.GetDirectories() |> Array.filter (fun d ->
      d.GetFiles("image.*").Length > 0 )
    if dirs.Length > 0 then
      let i = System.Random().Next(0, dirs.Length - 1)
      let path = sprintf "./Pages/%s/%s" (getDirId(dirs.[i]).Replace("_", "/")) (dirs.[i].GetFiles("image.*").[0].Name)
      path, dirs.[i]
    else
      "./Pages/_defaults/image.jpg", pagesDir.GetDirectories("_defaults").[0]

let getImgStr (dir: DirectoryInfo) (title: string) =
  let imgpath, cdir = tryGetMainImgSrc dir
  let css =
    match tryGet "thumbnail-css" cdir.FullName with
    | Some css -> sprintf "style='%s'" css
    | None -> ""
  sprintf "<img src='%s' %s alt='%s' />" imgpath css imgpath

let getBoxSetHtml (dirs: DirectoryInfo seq, cssId) =
  let htmls = dirs |> Seq.map (fun dir ->
    let id = getDirId dir
    let title = get "title" dir.FullName
    sprintf """
      <li>
      <a href="#%s" class='goto'>
        <h3>%s</h3>
        <div>
          <div class='cover'></div>
          %s
        </div>
      </a>
      </li>
    """ id title (getImgStr dir title))
  sprintf "<ul class='thumb-ul' %s>%s</ul>" cssId (htmls |> String.concat "\n")

let matchDirs (dirs: DirectoryInfo seq, regex) =
  dirs |> Seq.filter (fun dir ->
    let relPath = dir.FullName.Replace(pagesDir.FullName, "").Replace("\\", "/")
    let p = if relPath.Length > 0 then relPath.Remove(0, 1) else relPath
    Regex.IsMatch(p, regex) )

let getCssId (dic: IDictionary<string,string>) =
  if dic.ContainsKey "id" then
    let cid = dic.["id"].Trim()
    if cid.Length > 0 then sprintf "id='%s'" cid else ""
  else ""

let queryToDict (v: Match) =
  v.Groups.[1].Value.Split([|','|])
  |> Array.map (fun vi ->
    let vis = vi.Split [|':'|]
    vis.[0].Trim(), if vis.Length < 2 then "" else vis.[1].Trim()
  )
  |> dict

let sortDirs(dirs: DirectoryInfo seq) =
  dirs |> Seq.sortBy (fun dir ->
    match tryGet "index" dir.FullName with
    | Some i -> int i
    | None -> System.Int32.MaxValue
  )

let replaceBoxSet (dirs: DirectoryInfo seq) (text: string) =
  Regex.Replace(text, "{{(boxSet[^}]*)}}", fun v ->
    let dic = queryToDict v
    let cssId = getCssId dic
    let matchedDirs = matchDirs (dirs, dic.["boxSet"])
    getBoxSetHtml (sortDirs matchedDirs, cssId)
  )

let mergeStyles (text: string) =
  let sb = System.Text.StringBuilder()
  let text' = Regex.Replace(text, "<style>(.*?)</style>", (fun (m) -> 
    sb.AppendLine(m.Groups.[1].Value) |> ignore
    ""
  ), RegexOptions.Singleline)
  text'.Replace("</head>", "<style>\n" + sb.ToString() + "\n</style>\n</head>")  

let mergeScripts (text: string) =
  let sb = System.Text.StringBuilder()
  let text' = Regex.Replace(text, "<script>(.*?)</script>", (fun (m) -> 
    sb.AppendLine(m.Groups.[1].Value) |> ignore
    ""
  ), RegexOptions.Singleline)
  text'.Replace("</body>", "<script>\n" + sb.ToString() + "\n</script>\n</body>")  

let replaceDirs (text: string) =
  Regex.Replace(text, "{{ *dir *}}", fun v ->
    let m = Regex.Matches(text.Substring(0, v.Index), "<div id='([^']+)' class='page'>")
    let id = m.[m.Count - 1].Groups.[1].Value
    "Pages/" + id.Replace("_", "/") + "/"
  )

let replaces dirs (text: string) =
  text
  |> replaceDirs
  |> replaceBoxSet dirs
  |> mergeStyles
  |> mergeScripts

let rec getNavbar(dir: DirectoryInfo) =
  if dir.Parent.FullName.Contains pagesDir.FullName then
    match tryGet "title" dir.Parent.FullName with
    | Some parentTitle ->
      let t: string = getNavbar dir.Parent
      let v = if t.Length > 0 then t + "&#160;&gt;&#160;<wbr/>" else ""
      v + sprintf "<a class='mynav-link goto' href='#%s'>%s</a>" (getDirId dir.Parent) parentTitle
    | None -> getNavbar dir.Parent
  else ""

let dirToHtml(dir: DirectoryInfo) =
  let html = File.ReadAllText(Path.Combine(dir.FullName, "page.html"))
  let navbar =
    let res = (getNavbar dir).Trim()
    if res.Length > 0 then "<div class='mynav'>"+res+"</div>" else ""
  System.String.Format("""<div id='{0}' class='page'>
    <div class="header">
      <img src="./Content/images/logo.header.png" alt="logo" />
      <span class='searchButton'></span>
      <form class='searchWrapper'>
        <input type='text' placeholder="Søg..." value='' autocapitalize="off"/>
      </form>
      <h1>{2}</h1>
    </div>
    {1}
    <div class="main">
    {3}
    </div>
    <div class="trailer">
      <img src="Content/images/ti.name.black.en.png" alt="logo" />
      <div>
        Taastrup<br/>
        Gregersensvej 1<br/>
        DK-2630 Taastrup<br/>
        Denmark<br/>
        <div class="phoneprefix">Phone</div> +45 72 20 20 00<br/>
        <div class="phoneprefix">Fax</div> +45 72 20 20 19<br/>
      </div>
      <div>
        Aarhus, Teknologiparken<br/>
        Kongsvang Allé 29<br/>
        DK-8000 Aarhus C<br/>
        Denmark<br/>
        <div class="phoneprefix">Phone</div> +45 72 20 20 00<br/>
        <div class="phoneprefix">Fax</div> +45 72 20 10 19<br/>
      </div>
      <span class="clear"></span>
    </div>
    </div>""", getDirId dir, navbar, get "title" dir.FullName, html)

let writeHtml(dirs: DirectoryInfo seq) =
  let html = dirs |> Seq.map dirToHtml |> String.concat "\n\n<!-- ***************************************** -->\n\n"
  let mainHtml = File.ReadAllText(Path.Combine(root, "main.html"))
  let mainHtml' = mainHtml.Replace("<pages></pages>", html) |> replaces dirs
  
  let outFile = Path.Combine(outDir.FullName, "index.html")
  File.WriteAllText(outFile, mainHtml')
  let outFileNC = Path.Combine(outDir.FullName, "ncindex.html")
  File.WriteAllText(outFileNC, mainHtml'.Replace("manifest=\"./cache.manifest\"", ""))

let rec getAllPages (dirs: _ List) (dir: DirectoryInfo) =
  if File.Exists (Path.Combine(dir.FullName, "page.html")) then dirs.Add dir
  dir.GetDirectories() |> Array.iter (getAllPages dirs)

let rec copy (source, target, dirFilter, fileFilter, targetModifier) =
  let source, target = DirectoryInfo source, DirectoryInfo target
  for dir in source.GetDirectories() do
    if dirFilter dir then
      let subDir = Path.Combine(target.FullName, dir.Name)
      copy (dir.FullName, subDir, dirFilter, fileFilter, targetModifier)
  for file in source.GetFiles() do
    let targetFile = Path.Combine(target.FullName, file.Name)
    if fileFilter file && File.Exists targetFile = false then
      let newFileTarget = FileInfo(targetModifier targetFile)
      newFileTarget.Directory.Create()
      file.CopyTo(newFileTarget.FullName, true) |> ignore

let copyAssets() =
  copy (
    Path.Combine(root, "Scripts"),
    Path.Combine(outDir.FullName, "Scripts"),
    (fun d -> d.Name <> "typings"),
    (fun _ -> true),
    (id) )
  copy (
    Path.Combine(root, "Content"),
    Path.Combine(outDir.FullName, "Content"),
    (fun _ -> true),
    (fun _ -> true),
    (id) )
  let pagesOutDir = Path.Combine(outDir.FullName, "Pages\\")
  copy (
    pagesDir.FullName,
    pagesOutDir,
    (fun _ -> true),
    (fun f -> f.Name <> "page.html" && f.Name <> "meta.xml" ),
    (id) )
  File.Copy(Path.Combine(root, ".htaccess"), Path.Combine(outDir.FullName, ".htaccess"), true)

let constructCacheManifest() =
  let files = outDir.GetFiles("*", SearchOption.AllDirectories)
  let relFiles = files |> Array.choose (fun f ->
    if  f.Name <> ".htaccess" && f.Name <> "cache.manifest" && f.Name <> "ncindex.html" &&
        f.Extension <> ".avi" && f.Extension <> ".mp4" then
      let rel = f.FullName.Replace(outDir.FullName, "").Replace("\\","/")
      Some ("http://guides.musicope.com/" + what + rel)
    else None )
  let manFiles = relFiles |> String.concat "\r\n"
  let outFile =
    sprintf """CACHE MANIFEST
#version %A
%s
NETWORK:
*""" System.DateTime.Now manFiles
  File.WriteAllText(Path.Combine(outDir.FullName, "cache.manifest"), outFile)

let dirs = List()
getAllPages dirs pagesDir
writeHtml dirs
copyAssets()
constructCacheManifest()