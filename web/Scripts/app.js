/// <reference path="./references.ts" />
var App;
(function (App) {
    window.applicationCache.addEventListener('updateready', function (e) {
        window.applicationCache.swapCache();
    }, false);

    window.onerror = function (msg, url, linenumber) {
        var er = document.getElementById('alert');
        er.innerHTML = 'Error message:<br/>' + msg + '<br/><br/>URL:<br/>' + url + '<br/><br/>Line Number:<br/>' + linenumber;
        er.style.display = 'block';
        return true;
    };

    $(document).ready(function () {
        $('.img-desc').each(function (i, el) {
            $(el).after('<div class="img-desc-inner">' + $(el).attr('alt') + '</div>');
        });

        $('video[lazy-src]').each(function (i, el) {
            var p = $(el).parents('div[class="page"]');
            p.on('page-shown', function () {
                if (!$(el).attr('src')) {
                    $(el).attr('src', $(el).attr('lazy-src'));
                }
            });
        });
    });
})(App || (App = {}));
/// <reference path="./references.ts" />
var App;
(function (App) {
    var animates = {};

    function animateH1(h1, id) {
        if (h1.is(":visible") && h1[0].scrollHeight > h1.height()) {
            animates[id] = animates[id] ? (animates[id] + h1.height()) % h1[0].scrollHeight : h1.height();
            h1.delay(1000).animate({ 'scrollTop': animates[id] }, 1000, function () {
                animateH1(h1, id);
            });
        } else {
            setTimeout(function () {
                animateH1(h1, id);
            }, 1000);
        }
    }

    $(document).ready(function () {
        $('.page').each(function (i, el) {
            var id = $(el).attr('id');
            var h1 = $(el).find('.header > h1');
            animateH1(h1, id);
            return true;
        });
    });
})(App || (App = {}));
/// <reference path="./references.ts" />
var App;
(function (App) {
    function moveNavigationBar() {
        $('.navigate').css('left', document.documentElement.clientWidth / 2 - 65);
    }

    $(document).ready(function () {
        moveNavigationBar();
    });

    $(window).resize(function () {
        moveNavigationBar();
    });

    window.setInterval(function () {
        var zoom = document.documentElement.clientWidth / window.innerWidth;
        if (zoom > 1.5) {
            $('.navigate').hide();
        } else {
            $('.navigate').show();
        }
    }, 200);
})(App || (App = {}));
/// <reference path="./references.ts" />
var App;
(function (App) {
    var pageDef = $.Deferred().resolve();
    var scrolls = {};
    var firstLoad = true;
    var shouldAnimate = true;

    function fadeIn(newPageId, newPageParams, duration) {
        $(newPageId).css('opacity', 0).css('display', 'block');
        localStorage.setItem("lastPage", newPageId);
        $(newPageId).trigger('page-loading', newPageParams);
        $(window).scrollTop(scrolls[newPageId] || 0);
        $(newPageId).transition({ "opacity": 1 }, duration, function () {
            $(newPageId).trigger('page-shown', newPageParams);
            pageDef.resolve();
        });
    }

    function fadeOut(duration, doneFn) {
        var oldPageId = $('.page:visible');
        if (oldPageId.length === 0) {
            doneFn();
        } else {
            oldPageId.find('.searchWrapper > input').hide().val('');
            scrolls["#" + oldPageId.attr('id')] = $(window).scrollTop();
            oldPageId.transition({ "opacity": 0 }, duration, doneFn);
        }
    }

    function goto(newHash, notPushState) {
        var duration = shouldAnimate ? 500 : 0;
        newHash = newHash || "";
        pageDef.then(function () {
            pageDef = $.Deferred();
            var newHashData = newHash.split('/', 2);
            var newPageId = newHashData[0];
            if ($(newPageId).length === 0) {
                pageDef.resolve();
                goto("#home");
            } else {
                fadeOut(duration, function () {
                    $('.page:visible').hide();
                    if (!notPushState) {
                        history.pushState(newHash, '', newHash);
                    }
                    fadeIn(newPageId, newHashData[1], duration);
                });
            }
        });
    }

    window.onpopstate = function (e) {
        var newHash = firstLoad && navigator['standalone'] ? localStorage.getItem("lastPage") : window.location.hash;
        firstLoad = false;
        goto(newHash, true);
    };

    $(document).on('click', '.navigate-back', function (e) {
        history.back();
        e.preventDefault();
    });

    $(document).on('click', '.navigate-forward', function (e) {
        history.forward();
        e.preventDefault();
    });

    $(document).on('touchmove', function () {
        shouldAnimate = false;
    });

    $(document).on('touchstart', function () {
        shouldAnimate = true;
    });

    $(document).on('touchend', function () {
        shouldAnimate = true;
    });
})(App || (App = {}));
/// <reference path="./references.ts" />
var App;
(function (App) {
    function highlight(img) {
        if ($('body').scrollTop() !== img.offset().top) {
            $('body').animate({ scrollTop: img.offset().top }, 1000, function () {
                img.transition({ opacity: 0.2 }, 500).transition({ opacity: 1 }, 500);
            });
        } else {
            img.transition({ opacity: 0.2 }, 500).transition({ opacity: 1 }, 500);
        }
    }

    function linksToHighlights(pageId) {
        $('#' + pageId + ' .content a.figure').each(function (i, el) {
            var id = $(el).text().replace(' ', '-');
            $(el).click(function (e) {
                highlight($('#' + pageId + ' .content .' + id));
                e.preventDefault();
            });
        });
    }
    App.linksToHighlights = linksToHighlights;
})(App || (App = {}));
/// <reference path="./references.ts" />
var App;
(function (App) {
    //img link
    function resize(left, top, imgj, link) {
        return function (e) {
            var dx = imgj.offset().left - imgj.parent().offset().left;
            var x = left * imgj.width() + dx;
            var y = top * imgj.height();
            link.css('left', x);
            link.css('top', y);
            link.fadeIn(500);
        };
    }

    function createImgLink(img, pageId) {
        return function (left, top, text, url) {
            var imgj = $(img);
            var inner = url ? '<a class="imgLink goto" href = "' + url + '" >' + text + '</a>' : '<div class="imgMarker">' + text + '</div>';
            var link = $('<div class="imgLink-wrapper">' + inner + '</div>');
            var r = resize(left, top, imgj, link);
            imgj.before(link);
            $(window).resize(r);
            $(pageId).on('page-shown', r);
        };
    }
    App.createImgLink = createImgLink;
})(App || (App = {}));
/// <reference path="./references.ts" />
var App;
(function (App) {
    $(document).on('page-loading', '#search', function (e, search) {
        var search = decodeURIComponent(search);
        var searches = search.split(' ');
        var results = $('#search .results').html('');
        $('#search .title').html('Results for: <span>' + search + '</span>');
        $('.page:not(#search)').each(function (i, el) {
            var els = $(el).filter(function () {
                var curText = this.innerText.toUpperCase();
                var found = searches.some(function (search) {
                    return curText.indexOf(search.toUpperCase()) >= 0;
                });
                return found;
            });
            if (els.length > 0) {
                var title = $(el).find('.header > h1').text();
                var path = $(el).find('.mynav > a').map(function (i, el) {
                    return el.innerText;
                }).toArray().join('<wbr/>&#160;&gt;&#160;<wbr/>');
                if (path.length > 0) {
                    path = path + "<wbr/>&#160;&gt;&#160;<wbr/>";
                }
                results.append('<li><a class="goto" href="#' + $(el).attr('id') + '">' + path + '<span>' + title + '</span></a></li>');
            }
        });
    });

    //search
    function searchButton() {
        $('.page').each(function (i, el) {
            var input = $(el).find('.searchWrapper > input');
            input.focusout(function () {
                var query = input.val();
                if (query.replace(/^\s+|\s+$/g, '').length > 0) {
                    location.hash = "#search/" + encodeURIComponent(query);
                } else {
                    input.hide();
                    input.val('');
                }
            });
            $(el).find('.searchButton').click(function () {
                input.show();
                input.focus();
            });
            $(el).find('.searchWrapper').submit(function (e) {
                location.hash = "#search/" + encodeURIComponent($(el).find('.searchWrapper > input').val());
                e.preventDefault();
            });
        });
    }

    $(document).ready(function () {
        searchButton();
    });
})(App || (App = {}));
/// <reference path="./references.ts" />
var App;
(function (App) {
    var tooltipTimer;
    var id = 0;

    function textNodesUnder(root) {
        var n, a = [], walk = document.createTreeWalker(root, NodeFilter.SHOW_TEXT, null, false);
        while (n = walk.nextNode())
            a.push(n);
        return a;
    }

    function replaceText(search, replace) {
        var remove = [];
        $('.content').each(function (i, e) {
            var texts = textNodesUnder(e);
            texts.forEach(function (el) {
                var newVal = el.nodeValue.replace(new RegExp(search, 'g'), replace);
                if (newVal !== el.nodeValue) {
                    $(el).before(newVal);
                    remove.push(el);
                }
            });
        });
        $(remove).remove();
    }

    function showTooltip(text, delay) {
        var t = $('#tooltip');
        t.text(text).css('opacity', 0).show();
        t.transition({ 'opacity': 1 }, 1000, function () {
            if (tooltipTimer) {
                clearTimeout(tooltipTimer);
            }
            tooltipTimer = setTimeout(function () {
                $('#tooltip').transition({ 'opacity': 0 }, 1000, function () {
                    $('#tooltip').hide();
                });
            }, delay);
        });
    }

    $(document).on('click', '#tooltip', function () {
        if (tooltipTimer) {
            clearTimeout(tooltipTimer);
        }
        tooltipTimer = null;
        $('#tooltip').transition({ 'opacity': 0 }, 1000, function () {
            $('#tooltip').hide();
        });
    });

    function addTooltip(replaces, description, delay) {
        $(document).ready(function () {
            id++;
            var rs = typeof replaces === "string" ? [replaces] : replaces;
            rs.forEach(function (replace) {
                replaceText('(' + replace + ')', '<a href="#" class="tooltip-' + id + '">$1</a>');
            });
            $(document).on('click', '.tooltip-' + id, function (e) {
                showTooltip(description, delay || 10000);
                e.preventDefault();
            });
        });
    }
    App.addTooltip = addTooltip;
})(App || (App = {}));
//# sourceMappingURL=app.js.map
