/// <reference path="./references.ts" />

module App {

  $(document).on('page-loading', '#search', function (e, search) {
    var search = decodeURIComponent(search);
    var searches = search.split(' ');
    var results = $('#search .results').html('');
    $('#search .title').html('Results for: <span>' + search + '</span>');
    $('.page:not(#search)').each(function (i, el) {
      var els = $(el).filter(function () {
        var curText = this.innerText.toUpperCase();
        var found = searches.some(function (search) {
          return curText.indexOf(search.toUpperCase()) >= 0;
        });
        return found;
      });
      if (els.length > 0) {
        var title = $(el).find('.header > h1').text();
        var path = $(el).find('.mynav > a').map(function (i, el: HTMLElement) {
          return el.innerText;
        }).toArray().join('<wbr/>&#160;&gt;&#160;<wbr/>');
        if (path.length > 0) {
          path = path + "<wbr/>&#160;&gt;&#160;<wbr/>";
        }
        results.append('<li><a class="goto" href="#' + $(el).attr('id') + '">' + path + '<span>' + title + '</span></a></li>');
      }
    });
  });
  
  //search
  function searchButton() {
    $('.page').each((i, el) => {
      var input = $(el).find('.searchWrapper > input');
      input.focusout(() => {
        var query: string = input.val();
        if (query.replace(/^\s+|\s+$/g, '').length > 0) {
          location.hash = "#search/" + encodeURIComponent(query);
        } else {
          input.hide();
          input.val('');
        }
      });
      $(el).find('.searchButton').click(() => {
        input.show();
        input.focus();
      });
      $(el).find('.searchWrapper').submit((e) => {
        location.hash = "#search/" + encodeURIComponent($(el).find('.searchWrapper > input').val());
        e.preventDefault();
      });
    });
  }

  $(document).ready(() => {
    searchButton();
  });

}