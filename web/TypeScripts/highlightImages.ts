/// <reference path="./references.ts" />

module App {

  function highlight(img: JQuery) {
    if ($('body').scrollTop() !== img.offset().top) {
      $('body').animate({ scrollTop: img.offset().top }, 1000, function () {
        img.transition({ opacity: 0.2 }, 500).transition({ opacity: 1 }, 500);
      });
    } else {
      img.transition({ opacity: 0.2 }, 500).transition({ opacity: 1 }, 500);
    }
  }

  export function linksToHighlights(pageId: string) {
    $('#' + pageId + ' .content a.figure').each(function (i, el) {
      var id = $(el).text().replace(' ', '-');
      $(el).click(function (e) {
        highlight($('#' + pageId + ' .content .' + id));
        e.preventDefault();
      });
    });
  }


}