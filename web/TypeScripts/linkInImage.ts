/// <reference path="./references.ts" />

module App {

  //img link
  function resize(left: number, top: number, imgj: JQuery, link: JQuery) {
    return (e?) => {
      var dx = imgj.offset().left - imgj.parent().offset().left;
      var x = left * imgj.width() + dx;
      var y = top * imgj.height();
      link.css('left', x);
      link.css('top', y);
      link.fadeIn(500);
    };
  }

  export function createImgLink(img, pageId) {
    return (left, top, text, url) => {
      var imgj = $(img);
      var inner = url ? '<a class="imgLink goto" href = "' + url + '" >' + text + '</a>' : '<div class="imgMarker">' + text + '</div>';
      var link = $('<div class="imgLink-wrapper">' + inner + '</div>');
      var r = resize(left, top, imgj, link);
      imgj.before(link);
      $(window).resize(r);
      $(pageId).on('page-shown', r);
    };
  }

}