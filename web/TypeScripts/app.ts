/// <reference path="./references.ts" />

module App {

  window.applicationCache.addEventListener('updateready', (e) => { window.applicationCache.swapCache(); }, false);

  window.onerror = function (msg, url, linenumber) {
    var er = document.getElementById('alert');
    er.innerHTML = 'Error message:<br/>' + msg + '<br/><br/>URL:<br/>' + url + '<br/><br/>Line Number:<br/>' + linenumber
    er.style.display = 'block';
    return true;
  }

  $(document).ready(() => {

    $('.img-desc').each((i, el) => {
      $(el).after('<div class="img-desc-inner">' + $(el).attr('alt') + '</div>');
    });

    $('video[lazy-src]').each((i, el) => {
      var p = $(el).parents('div[class="page"]');
      p.on('page-shown', () => {
        if (!$(el).attr('src')) {
          $(el).attr('src', $(el).attr('lazy-src'));
        }
      });
    });

  });

}