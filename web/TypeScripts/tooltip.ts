/// <reference path="./references.ts" />

module App {

  var tooltipTimer;
  var id = 0;

  function textNodesUnder(root): Node[] {
    var n, a = [], walk = document.createTreeWalker(root, NodeFilter.SHOW_TEXT, null, false);
    while (n = walk.nextNode()) a.push(n);
    return a;
  }

  function replaceText(search, replace) {
    var remove = [];
    $('.content').each((i, e) => {
      var texts = textNodesUnder(e);
      texts.forEach((el) => {
        var newVal = el.nodeValue.replace(new RegExp(search, 'g'), replace);
        if (newVal !== el.nodeValue) {
          $(el).before(newVal);
          remove.push(el);
        }
      });
    });
    $(remove).remove();
  }
  
  function showTooltip(text: string, delay: number) {
    var t = $('#tooltip');
    t.text(text).css('opacity', 0).show();
    t.transition({ 'opacity': 1 }, 1000, () => {
      if (tooltipTimer) { clearTimeout(tooltipTimer); }
      tooltipTimer = setTimeout(() => { $('#tooltip').transition({ 'opacity': 0 }, 1000, () => { $('#tooltip').hide(); }); }, delay);
    });
  }

  $(document).on('click', '#tooltip', () => {
    if (tooltipTimer) { clearTimeout(tooltipTimer); }
    tooltipTimer = null;
    $('#tooltip').transition({ 'opacity': 0 }, 1000, () => { $('#tooltip').hide(); });
  });

  export function addTooltip(replaces, description: string, delay?: number) {
    $(document).ready(() => {
      id++;
      var rs = typeof replaces === "string" ? [replaces] : replaces;
      rs.forEach((replace) => {
        replaceText('(' + replace + ')', '<a href="#" class="tooltip-' + id + '">$1</a>');
      });
      $(document).on('click', '.tooltip-' + id, function (e) {
        showTooltip(description, delay || 10000);
        e.preventDefault();
      });
    });
  }


}