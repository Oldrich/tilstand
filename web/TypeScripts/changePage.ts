/// <reference path="./references.ts" />

module App {

  var pageDef = $.Deferred().resolve();
  var scrolls = {};
  var firstLoad = true;
  var shouldAnimate = true;

  function fadeIn(newPageId: string, newPageParams: string, duration: number) {
    $(newPageId).css('opacity', 0).css('display', 'block');
    localStorage.setItem("lastPage", newPageId);
    $(newPageId).trigger('page-loading', newPageParams);
    $(window).scrollTop(scrolls[newPageId] || 0);
    $(newPageId).transition({"opacity": 1}, duration, () => {
      $(newPageId).trigger('page-shown', newPageParams);
      pageDef.resolve();
    });
  }

  function fadeOut(duration: number, doneFn) {
    var oldPageId = $('.page:visible');
    if (oldPageId.length === 0) {
      doneFn();
    } else {
      oldPageId.find('.searchWrapper > input').hide().val('');
      scrolls["#" + oldPageId.attr('id')] = $(window).scrollTop();
      oldPageId.transition({ "opacity": 0 }, duration, doneFn);
    }
  }

  function goto(newHash: string, notPushState?: boolean) {
    var duration = shouldAnimate ? 500 : 0;
    newHash = newHash || "";
    pageDef.then(() => {
      pageDef = $.Deferred();
      var newHashData = newHash.split('/', 2);
      var newPageId = newHashData[0];
      if ($(newPageId).length === 0) {
        pageDef.resolve();
        goto("#home");
      } else {
        fadeOut(duration, () => {
          $('.page:visible').hide();
          if (!notPushState) {
            history.pushState(newHash, '', newHash);
          }
          fadeIn(newPageId, newHashData[1], duration);
        });
      }
    });
  }

  window.onpopstate = function (e) {
    var newHash = firstLoad && navigator['standalone'] ? localStorage.getItem("lastPage") : window.location.hash;
    firstLoad = false;
    goto(newHash, true);
  }

  $(document).on('click', '.navigate-back', (e) => {
    history.back();
    e.preventDefault();
  });

  $(document).on('click', '.navigate-forward', (e) => {
    history.forward();
    e.preventDefault();
  });

  $(document).on('touchmove', () => {
    shouldAnimate = false;
  });

  $(document).on('touchstart', () => {
    shouldAnimate = true;
  });

  $(document).on('touchend', () => {
    shouldAnimate = true;
  });

}