/// <reference path="./references.ts" />

module App {

  function moveNavigationBar() {
    $('.navigate').css('left', document.documentElement.clientWidth / 2 - 65);
  }

  $(document).ready(() => {
    moveNavigationBar();
  });

  $(window).resize(() => {
    moveNavigationBar();
  });

  window.setInterval(() => {
    var zoom = document.documentElement.clientWidth / window.innerWidth;
    if (zoom > 1.5) {
      $('.navigate').hide();
    } else {
      $('.navigate').show();
    }
  }, 200);

}