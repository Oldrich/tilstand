/// <reference path="./references.ts" />

module App {

  var animates = {};

  function animateH1(h1: JQuery, id: string) {
    if (h1.is(":visible") && h1[0].scrollHeight > h1.height()) {
      animates[id] = animates[id] ? (animates[id] + h1.height()) % h1[0].scrollHeight : h1.height();
      h1.delay(1000).animate({ 'scrollTop': animates[id] }, 1000, () => { animateH1(h1, id); });
    } else {
      setTimeout(() => { animateH1(h1, id); }, 1000);
    }
  }

  $(document).ready(() => {
    $('.page').each((i, el) => {
      var id = $(el).attr('id');
      var h1 = $(el).find('.header > h1');
      animateH1(h1, id);
      return true;
    });
  });

}